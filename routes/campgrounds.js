var express = require("express");
var router = express.Router();
var Campground = require("../models/campground");
var middleware = require("../middleware");

router.get("/", function(req, res){
    Campground.find({}, function(err, allCampgrounds){
        if(err){
            console.log(err);
        } else{
            res.render("campgrounds/index", {camp:allCampgrounds, currentUser: req.user})
        }
    })
    //res.render("campgrounds", {camp:camp});
});

router.post("/", middleware.isLoggedIn ,function(req, res){
    var name = req.body.name;
    var image =req.body.image;
    var desc = req.body.description;
    var author = {
        id: req.user._id,
        username: req.user.username
    }
    var newCamp = {name: name, image: image, description:desc, author:author}
    Campground.create(newCamp, function(err, newlyCreated){
        if(err){
            console.log(err);
        } else {
            console.log(newlyCreated);
            res.redirect("/campgrounds");
        }
    });
});

router.get("/new", middleware.isLoggedIn, function(req, res) {
   res.render("campgrounds/new"); 
});

router.get("/:id", function(req, res) {
    //find camp ground
    Campground.findById(req.params.id).populate("comments").exec(function(err, foundCampground){
        if(err){
            console.log(err);
        } else {
            res.render("campgrounds/show", {campground:foundCampground});
        }
    });
});

// edit campground route

router.get("/:id/edit", middleware.checkCampgroundOwnership ,function(req, res) {
    // is user logged in
        Campground.findById(req.params.id, function(err, foundCampground){
            res.render("campgrounds/edit", {campground: foundCampground});
    });
});

//update camground route

router.put("/:id", middleware.checkCampgroundOwnership, function(req, res){
    // find the correct camp groun d
    
    Campground.findOneAndUpdate(req.params.id, req.body.campground, function (err, updatedCampground){
        if(err){
            res.redirect("/campgrounds");
        } else {
            res.redirect("/campgrounds/" + req.params.id);
        }
    });

    //redirect somewhere show case
});

// destroy campground route 

router.delete("/:id", middleware.checkCampgroundOwnership, function (req, res){
    Campground.findOneAndDelete(req.params.id, function(err){
        if(err){
            res.redirect("/campgrounds");
        } else {
            res.redirect("/campgrounds");
        }
    });
});
  
module.exports=router;